const path = require('path')

module.exports = {
  themes: {
    development: {
      id: '80538337355',
      password: '04543cd06432025e210d795bd4841af0',
      store: 'fromourplace-com.myshopify.com',
      ignore: [
        'settings_data.json' // leave this here to avoid overriding theme settings on sync
      ]
    }
  }
}
