import { component } from 'picoapp'
import 'slick-carousel'
import $ from 'jquery'

export default component((node, ctx) => {
  $('.js-press-quotes-container').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    autoplay: true,
    autoplaySpeed: 3000,
    asNavFor: '.js-press-logos-container',
    responsive: [
      {
        breakpoint: 745,
        settings: {
          fade: false
        }
      }
    ]
  });

  $('.js-press-logos-container').slick({
    slidesToShow: 4,
    slidesToScroll: 4,
    asNavFor: '.js-press-quotes-container',
    dots: false,
    variableWidth: true,
    centerMode: true,
    infinite: true,
    arrows: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 745,
        settings: {
          slidesToShow: 1,
          variableWidth: false,
          dots: true,
          fade: true,
        }
      }
    ]
  });

})
