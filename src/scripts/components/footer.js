import { component } from 'picoapp'
import subscribe from 'klaviyo-subscribe'

export default component((node, ctx) => {

  var credits_toggle = node.querySelector('.js-credits-toggle')
  var credits = node.querySelector('.js-credits')
  var credits_cover = node.querySelector('.js-credits-cover')

  credits_cover.addEventListener('click', e => {
    e.preventDefault()
    credits.classList.remove('active')
    credits_cover.classList.remove('active')
  })

  credits_toggle.addEventListener('click', e => {
    e.preventDefault()
    credits.classList.toggle('active')
    credits_cover.classList.add('active')
  })

  const form = node.querySelector('.footer__form')

  const listId = form.getAttribute('data-list-id')

  form.addEventListener('submit', e => {
    e.preventDefault()

    const email = e.target.elements.email.value

    if (!email) return

    subscribe(listId, email, {}).then(res => {
      form.classList.add('was-successful')
      form.reset()

      setTimeout(() => {
        // form.classList.remove('was-successful')
      }, 2000)
    })
  })

})
