import { component } from 'picoapp'
import { addVariant } from '@/lib/cart.js'
// import Accordion from 'accordion-js';
import Flickity from 'flickity'
import $ from 'jquery'

export default component((node, ctx) => {

  $('.js-accordion-toggle').click(function() {
    $(this).next('.js-accordion-body').slideToggle();
    $(this).toggleClass('open')
  })

})
