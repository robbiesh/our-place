import { component } from 'picoapp'
import Flickity from 'flickity'

export default component((node, ctx) => {

  var carousel = new Flickity( '.js-instagram', {
    // options
    wrapAround: true,
    percentagePosition: false,
    imagesLoaded: true,
    initialIndex: 5,
    freeScroll: true,
    accessibility: false,
    draggable: true
  });


})
