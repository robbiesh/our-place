import { component } from 'picoapp'

import $ from 'jquery'
import 'slick-carousel'

import {TweenMax, Power2, TimelineLite} from "gsap/TweenMax"
import ScrollMagic from 'scrollmagic'
import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'


export default component((node, ctx) => {

  // var slider = new Flickity( '.js-slider', {
  //   // options
  //   cellAlign: 'left',
  //   contain: true,
  //   adaptiveHeight: true,
  //   imagesLoaded: true,
  //   fade: true
  // });

  var slider = node.querySelector('.js-slider')

  if (slider) {
    $(slider).slick({
      dots: true,
      arrows: false,
      draggable: true,
      swipe: true,
      fade: true
    });
  }





  // var sliderScrollPos = node.offsetTop
  //
  // window.addEventListener('scroll', function() {
  //
	// 	var currentScrollPos = window.pageYOffset;
  //
	// 	if (currentScrollPos > sliderScrollPos) {
  //     console.log('here we are')
  //   }
	// })

  // var tween = TweenMax.to("#slide-image", 1, {scale: 1.2});
  //
  // var controller = new ScrollMagic.Controller();
  //
  // new ScrollMagic.Scene({triggerElement: "#slider", duration: "300%", offset: "-500"})
  //   .setTween(tween)
	// 	// .setClassToggle("#slider", "active") // add class toggle
  //   // .triggerHook(0)
	// 	.addTo(controller);



})
