import { component } from 'picoapp'
import $ from 'jquery'

export default component((node, ctx) => {
  let updateReviewsLoop = setInterval(function(){
    if ($(node).find(".sr-only")){
      let starRating = $(node).find(".sr-only").text().split(" ")[0]
      let reviewsCount = $(node).find("a.text-m").text();
      $(node).find("a.text-m").text(`${starRating} (${reviewsCount})`);
      clearInterval(updateReviewsLoop);
    }
  }, 1000);
})
