import { component } from 'picoapp'
import Flickity from 'flickity'
import FlickityAsNavFor from 'flickity-as-nav-for'
import FlickityFade from 'flickity-fade'

export default component((node, ctx) => {



  var slideIndex = node.querySelector('.about-slide-index');
  var timer = node.querySelector('.timer');

  var slider = new Flickity( '.js-about-slider', {
    fade: true,
    contain: true,
    pageDots: false,
    draggable: true,
    wrapAround: true,
    autoPlay: 8000,
    pauseAutoPlayOnHover: false
  });

  setTimeout(() => {
    slider.pausePlayer();
  }, 500)

  // node.querySelector('.js-about-slider').addEventListener('touchmove', function(e) { e.preventDefault(); }, { passive: false });
  //
  // slider.on('dragStart', () => (document.ontouchmove = e => e.preventDefault()));
  // slider.on('dragEnd', () => (document.ontouchmove = () => true));

  var image_slider = new Flickity( '.js-image-slider', {
    // options
    asNavFor: '.js-about-slider',
    contain: true,
    pageDots: false,
    wrapAround: true,
    draggable: false,
    fade: true,
    cellAlign: 'left'
  });



  window.addEventListener('scroll', function() {

		var currentScrollPos = window.pageYOffset;

    var aboutSlider = document.getElementById('about-slider')
    var sliderScrollPos = ''

    if (aboutSlider) {
      sliderScrollPos = aboutSlider.offsetTop - window.innerHeight
    }


    // console.log(currentScrollPos)
    // console.log(sliderScrollPos)

    var has_played = true

		if (currentScrollPos > sliderScrollPos) {
      if (has_played) {

        slider.unpausePlayer()
        timer.classList.add('timer--animated')
        has_played = false
      }
		}

	})

  // node.querySelector('.js-image-slider').addEventListener('touchmove', function(e) { e.preventDefault(); }, { passive: false });
  //
  // image_slider.on('dragStart', () => (document.ontouchmove = e => e.preventDefault()));
  // image_slider.on('dragEnd', () => (document.ontouchmove = () => true));

  const slides = node.querySelectorAll('.offset-image');

  image_slider.on( 'change', function( index ) {
    var j = index + 1
    slideIndex.innerHTML = j
    // console.log()

    timer.classList.remove('timer--animated')
    void timer.offsetWidth;
    timer.classList.add('timer--animated')

    for (let i = 0; i < slides.length; i++) {
      slides[i].classList.remove('is-selected')
    }

    node.querySelector('.offset-image-' + j).classList.add('is-selected')


  });

  image_slider.on('select', function(index) {
    slider.playPlayer();
  })


})
