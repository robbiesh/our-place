import { component } from 'picoapp'
import $ from 'jquery'
import app from '@/app.js'
import { fetchCart } from '@/lib/cart.js'
import cartDrawer from '@/components/cartDrawer.js'

import Flickity from 'flickity'
import FlickityAsNavFor from 'flickity-as-nav-for'
import FlickityFade from 'flickity-fade'
import FlickityImagesLoaded from 'flickity-imagesloaded'

import 'slick-carousel'

export default component((node, ctx) => {

  // const bundles = [ 'table-runner', 'tortilla-warmers', 'maria-bowl', 'rojas-molcajete', 'mezcal-glasses'];
  // var checkedValue = null;
  // var inputElements = document.getElementsByClassName('pan');

  // for (var j = 0; j < bundles.length; j++) {
  //
  //   var inputElements = document.getElementsByClassName(bundles[j]);
  //
  //   for (var i = 0; inputElements[i]; ++i) {
  //     if (inputElements[i].checked) {
  //        checkedValue = inputElements[i].value;
  //        break;
  //     }
  //   }
  //
  //   document.getElementById('js-' + bundles[j]).innerHTML = checkedValue;
  // }

  var slider = node.querySelector('.js-pdp-slider')
  var flkty = ''

  $('.js-pdp-slider').slick({
    dots: false,
    arrows: true,
    draggable: true,
    swipe: true,
    prevArrow: $('.prev'),
    nextArrow: $('.next')
  });

  $(".js-bundle-slider").slick({
    dots: false,
    arrows: false,
    draggable: true,
    swipe: true,
    adaptiveHeight: true
  });

  const checkboxes = document.querySelectorAll('.checkbox');

  console.log(checkboxes)
  //
  // for (let i = 0; i < checkboxes.length; i++) {
  //
  //   checkboxes[i].addEventListener('change', (event) => {
  //
  //     var id = event.srcElement.id
  //     var index = node.querySelector('.pdp-slide--' + id).dataset.index
  //
  //     $(".js-pdp-slider").slick('slickGoTo', index);
  //
  //     var n = event.target.getAttribute('name');
  //
  //     // $(".js-bundle-slider-" + n).slick('slickGoTo', 1);
  //     $(".js-bundle-slider-" + n).slick('slickGoTo', node.querySelector('#v-' + event.target.dataset.vid).dataset.index);
  //
  //     console.log(n);
  //
  //     var inputElements = document.getElementsByClassName(n);
  //
  //     for (var i = 0; inputElements[i]; ++i) {
  //       if (inputElements[i].checked) {
  //          checkedValue = inputElements[i].value;
  //          break;
  //       }
  //     }
  //
  //     document.getElementById('js-' + n).innerHTML = checkedValue;
  //
  //
  //     // console.log(name)
  //
  //   })
  // }

  const addBundle = node.querySelector('.js-add-bundle')


  addBundle.addEventListener('click', e => {
    e.preventDefault()

    var prodArr = [];

    for (let i = 0; i < checkboxes.length; i++) {
      prodArr.push(checkboxes[i].value)
    }

    console.log('are we here')
    console.log(prodArr)

    addAllItems(prodArr);

  })


  function addAllItems(array) {

    Shopify.queue = [];

    var quantity = 0;
    var newArray = array;

    for (var i = 0; i < newArray.length; i++) {
        var product = newArray[i]
        Shopify.queue.push({
            variantId: product
        });
    }

    Shopify.moveAlong = function() {
          // If we still have requests in the queue, let's process the next one.
          if (Shopify.queue.length) {
            var request = Shopify.queue.shift();
            var data = 'id=' + request.variantId + '&quantity=1'
            $.ajax({
              type: 'POST',
              url: '/cart/add.js',
              dataType: 'json',
              data: data,
              success: function(res) {
                  Shopify.moveAlong();
              },
              error: function() {
                  // if it's not last one Move Along else update the cart number with the current quantity
                  if (Shopify.queue.length) {
                      Shopify.moveAlong()
                  } else {
                      // $('#cart-number').replaceWith("<a href=" / cart " id="
                      //     cart - number ">View cart (" + quantity + ")</a>")
                  }
              }
            });
          }
          // If the queue is empty, we add 1 to cart
          else {
            quantity += 1;
            addToCartOk(quantity);
          }
      }
      Shopify.moveAlong();
  }

  function addToCartOk() {
      // open(ctx.getState().cart)
      return fetchCart().then(cart => {
        app.hydrate({ cart: cart })
        app.emit('cart:updated')
        app.emit('cart:toggle', state => {
          return {
            cartOpen: !state.cartOpen
          }
        })
        // app.emit('updated', { item, cart })
      })
  }

})
