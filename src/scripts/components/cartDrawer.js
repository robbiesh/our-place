import { component } from 'picoapp'
import { getSizedImageUrl, imageSize } from '@/lib/images.js'
import { formatMoney } from '@/lib/currency.js'
import app from '@/app.js'

const X = `<svg viewBox="0 0 16 16" width="16" height="16" fill="none" stroke="currentcolor" stroke-width="3" style="display:inline-block;vertical-align:middle;overflow:visible;"><path d="M1.0606601717798212 1.0606601717798212 L14.939339828220179 14.939339828220179"></path><path d="M14.939339828220179 1.0606601717798212 L1.0606601717798212 14.939339828220179"></path></svg>`

const cutouts = JSON.parse(document.querySelector('.js-cutouts').innerHTML)
const subtitles = JSON.parse(document.querySelector('.js-subtitles').innerHTML)
const shipping_notes = JSON.parse(document.querySelector('.js-shipping-notes').innerHTML)

// console.log(subtitles)

function handleize (str) {
    return str.toLowerCase().replace(/[^\w\u00C0-\u024f]+/g, "-").replace(/^-+|-+$/g, "");
};

function createItem ({
  variant_id: id,
  product_title: title,
  line_price: price,
  variant_title: color,
  image,
  url,
  quantity,
  ...item
}) {
  const img = image ? getSizedImageUrl(
    image.replace('_' + imageSize(image), ''), '200x' // TODO hacky af
  ) : 'https://source.unsplash.com/R9OS29xJb-8/2000x1333'

  var cutout_img = '';

  cutouts.variants.forEach(function(el) {

    // console.log(el.id)
    // console.log(id)

    if (el.id == id) {
      cutout_img = el.image
    }
  })

  var shipping_note = '';

  shipping_notes.variants.forEach(function(el) {
    if (el.id == id) {
      shipping_note = el.shipping_note
    }
  })

  var subtitle = ""
  subtitles.products.forEach(function(el) {
    if (el.title == title) {
      subtitle = el.subtitle
    }
  })

  var h = handleize(title)

  var label = ''

  if (h === 'gift-card') {
      label = 'Price'
  } else {
      label = 'Color'
  }

  return `
<div class='cart-drawer__item' data-component='cartDrawerItem' data-id=${id}>
  <div class='f aic'>
    <div class='cart-drawer__item-image'>
      <a href='${url}'>
        <img src='${cutout_img}' class='cutout-${h}' />
      </a>
    </div>
    <div class='__content pl1 f y ais jcb x'>
      <div class='x'>
        <div class='f jcb x'>
          <div class='fa'>
            <a href='${url}' class='cheltenham-x ls--10 fs-24 block lh-10 mb-2'>${title}</a>
            ${subtitle ? `${subtitle} ` : ``}
            ${color ? `<div class='block fs-14 plaid lh-14 ls--2 mb-4'>${label}: <span>${color.split(':')[0]}</span></div>` : ``}
            <div class='f aic x cart-selectors bold lh-10 fs-10 plaid-l'>
              <div class='cart-quantity js-remove-single p-1 bg-dark-beige mr-2'><div>-</div></div>
              <div class='js-single-quantity bg-dark-beige p-1'>${quantity}</div>
              <div class='cart-quantity js-add-single p-1 bg-dark-beige ml-2'><div>+</div></div>
            </div>

            <div class='x pt-2 pr-2'>
                ${shipping_note}
            </div>
          </div>
          <div class='f fdc jcb'>
            <button class='button--reset js-remove-item'>${X}</button>

            <div>${formatMoney(price, '${{amount_no_decimals}}')}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
`
}

function renderItems (cart, items) {

  var m = `<header class='pb-6 x z4 rel'><div class='x f aic jcb'><div><h3 class='mv0 serif cheltenham-x ls--10 fs-35 lh-10'>Your Bag</h3></div><div><div class='close rel js-close'><span class="fs-10 plaid-l ls--5 caps js-mm-close cursor--pointer">Close</span></div></div></div></header><div class='cart-drawer__items x fa z4 rel overflow--scroll pb-15'>`
  var n = `</div><div class='x z5 cart-drawer--bottom bg-beige rel'><div class='z6 rel x'><div class='x f jcb aic fs-14 mb-4 plaid-l ls--5'><div class='caps'>Subtotal</div><div>` + formatMoney(cart.total_price, '${{amount_no_decimals}}') + `</div></div><div class='x'><a href='https://fromourplace.com/checkout' class='js-checkout button p-2 plaid-l c-beige h-bg-gray transition bg-gray x block rel fs-14 caps mb-4 ac block'>Continue to Checkout</a></div><div class='x ac'>Free shipping on all orders <span class='cheltenham-x inline'>☺︎</span></div></div></div>`

  var r = `<div class='fa x'><div class='f jcb x'><p class='lg--fs-48 sm--fs-42 lh-12 ls--10 cheltenham-x c-gray lg--pr-4 eleven-twelfths'>Your bag is currently empty. Go get something nice for your place.</p><div class='close rel js-close'></div></div></div><div class='x ac'><div class='block mb-4'><a href='/collections/all' class='js-navigate-away button p-2 plaid-l c-beige h-bg-gray transition bg-gray x block rel fs-14 caps'>Shop the Collection</a></div><div class='block'>Free shipping on all orders <span class='cheltenham-x inline'>☺︎</span></div></div>`


  if ( items.length > 0 ) {
    var i = ''
    items.reduce((markup, item) => {

      i += createItem(item)


    }, '')

    m = m + i + n
    return m

  } else {
    return r
  }
}

export default component((node, ctx) => {
  const overlay = node.querySelector('.js-overlay')
  const closeButton = node.querySelector('.js-close')
  const subtotal = node.querySelector('.js-subtotal')
  const itemsRoot = node.querySelector('.js-items')
  const loading = itemsRoot.innerHTML

  const render = (cart) => {
    itemsRoot.innerHTML = renderItems(cart, cart.items)



    if (cart.items.length > 0) {
      node.classList.add('bg--hidden')
    } else {
      node.classList.remove('bg--hidden')
    }
    // subtotal.innerHTML = formatMoney(cart.total_price)

    if (node.querySelector('.js-close')) {
      node.querySelector('.js-close').addEventListener('click', close)
    }

    if (node.querySelector('.js-navigate-away')) {
      node.querySelector('.js-navigate-away').addEventListener('click', close)
    }

    // if (node.querySelector('.js-checkout')) {
    //   node.querySelector('.js-checkout').addEventListener('click', function(e) {
    //     e.preventDefault()
    //     window.location.href = node.querySelector('.js-checkout').href
    //   })
    // }
  }

  const open = (cart) => {

    const scrollY = document.documentElement.style.getPropertyValue('--scroll-y');
    const body = document.body;
    body.style.position = 'fixed';
    body.style.top = `-${scrollY}`;

    document.body.classList.add('cart-drawer--open')
    node.classList.add('is-active')
    itemsRoot.innerHTML = loading
    setTimeout(() => {
      node.classList.add('is-visible')
      setTimeout(render(cart), 10)
      app.mount()
    }, 50)
  }

  const close = () => {

    const body = document.body;
    const scrollY = body.style.top;
    body.style.position = '';
    body.style.top = '';
    window.scrollTo(0, parseInt(scrollY || '0') * -1);

    document.body.classList.remove('cart-drawer--open')
    node.classList.remove('is-visible')
    setTimeout(() => {
      node.classList.remove('is-active')
      app.hydrate({cartOpen: false})
    }, 400)
  }

  render(ctx.getState().cart)

  overlay.addEventListener('click', close)

  ctx.on('cart:toggle', ({ cart, cartOpen }) => {
    cartOpen && open(cart)
  })
  ctx.on('cart:updated', ({ cart, cartOpen }) => {
    render(ctx.getState().cart)
    app.mount()
  })
})
