import { component } from 'picoapp'
import Player from '@vimeo/player'

export default component((node, ctx) => {

  const player_id = node.querySelector('.js-video')

  if (player_id) {
    const player = new Player(player_id.id, {
      id: player_id.dataset.id,
      controls: false,
      height: player_id.dataset.height,
      muted: true,
      loop: true,
      autoplay: true,
      background: true
    })

    const overlay = node.querySelector('.js-overlay')

    player.on('loaded', function() {
      // data is an object containing properties specific to that event
      // setTimeout(() => player_container.classList.add('video--visible'), 2000)
      // console.log(data)

      setTimeout(() => player_id.classList.remove('pdp-video--hidden'), 2000)



      setTimeout(() => {
        player.play()
      }, 200)
    })

  }

})
