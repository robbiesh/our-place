import { component } from 'picoapp'
import Player from '@vimeo/player'

export default component((node, ctx) => {

  var slide = document.querySelector(".js-newsletter-slide")
  var trigger = node.querySelector(".js-trigger-newsletter")

  trigger.addEventListener('click', () => {
    slide.classList.add('visible')
  })

  const vid_id = node.querySelector('.js-video')



  if (vid_id) {
    const player_vid = new Player(vid_id.id, {
      id: vid_id.dataset.id,
      controls: false,
      height: vid_id.dataset.height,
      muted: true,
      loop: true,
      autoplay: true,
      background: true
    })

    player_vid.on('loaded', function() {
      // data is an object containing properties specific to that event
      setTimeout(() => vid_id.classList.remove('pdp-video--hidden'), 2000)

      setTimeout(() => {
        player_vid.play()
      }, 500)

    })

  }

})
