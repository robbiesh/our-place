import { component } from 'picoapp'
import cookie from 'js-cookie'
// import router from '@/router.js'

export default component(( node, ctx ) => {
  const closeNewsletter = node.querySelector('.js-hide-newsletter')
  const visible = cookie.get('newsletter')
  const form = node.querySelector('form')

  if (!visible) {
    setTimeout(() => {
      node.classList.add('visible')
    }, 10000)
  }
  if (closeNewsletter) {
    closeNewsletter.addEventListener('click', () => {
      node.classList.remove('visible')
      cookie.set('newsletter', true)
    })
  }
  form.addEventListener('submit', () => {
    setTimeout(() => {
      node.classList.remove('visible')
      cookie.set('newsletter', true)
    }, 1000)
  })
})
