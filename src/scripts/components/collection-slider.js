import { component } from 'picoapp'
import Flickity from 'flickity'
import FlickityAsNavFor from 'flickity-as-nav-for'
import FlickityFade from 'flickity-fade'

export default component((node, ctx) => {

  var slider = new Flickity( '.js-collection-slider', {
    // options
    wrapAround: true,
    draggable: true,
    pauseAutoPlayOnHover: false,
    autoPlay: true
  });

  var image_slider = new Flickity( '.js-collection-image-slider', {
    // options
    asNavFor: '.js-collection-slider',
    fade: true,
    wrapAround: true
  });

})
