import { component } from 'picoapp'
import Isotope from 'isotope-layout'
import imagesLoaded from 'imagesloaded'
import $ from 'jquery'


export default component((node, ctx) => {

  // external js: isotope.pkgd.js

  var grid = '';

  // init Isotope



  // multiple elements
  var grid_items = document.querySelectorAll('.grid__item');

  imagesLoaded( grid_items, function() {
    grid = new Isotope( '.grid', {
      itemSelector: '.grid__item',
      layoutMode: 'fitRows'
    });
  });



  // bind filter button click
  var filtersElem = document.querySelector('.filters');

  filtersElem.addEventListener( 'click', function( event ) {
    // only work with buttons
    // if ( !matchesSelector( event.target, 'button' ) ) {
    //   return;
    // }

    var filterValue = event.target.getAttribute('data-filter');

    location.hash = 'filter=' + encodeURIComponent( filterValue );
  });

  // change is-checked class on buttons
  var buttonGroups = document.querySelectorAll('.filters');
  var $filterButtonGroup = $('.filter-button-group');

  for ( var i=0, len = buttonGroups.length; i < len; i++ ) {
    var buttonGroup = buttonGroups[i];
    radioButtonGroup( buttonGroup );
  }

  function radioButtonGroup( buttonGroup ) {
    buttonGroup.addEventListener( 'click', function( event ) {
      // only work with buttons
      // if ( !matchesSelector( event.target, 'button' ) ) {
      //   return;
      // }


      buttonGroup.querySelector('.is-checked').classList.remove('is-checked');
      event.target.classList.add('is-checked');
    });
  }

  function getHashFilter() {
    // get filter=filterName
    var matches = location.hash.match( /filter=([^&]+)/i );
    var hashFilter = matches && matches[1];
    return hashFilter && decodeURIComponent( hashFilter );
  }

  var isIsotopeInit = false;

  function onHashchange() {
    var hashFilter = getHashFilter();
    // if ( !hashFilter && isIsotopeInit ) {
    //
    // }

    if (hashFilter) {

      $filterButtonGroup.find('.is-checked').removeClass('is-checked');
      $filterButtonGroup.find('[data-filter="' + hashFilter + '"]').addClass('is-checked');

      if (!isIsotopeInit) {
        imagesLoaded( grid_items, function() {
          grid = new Isotope( '.grid', {
            itemSelector: '.grid__item',
            layoutMode: 'fitRows',
            filter: hashFilter
          });
        });

        isIsotopeInit = true;

      } else {
        grid.arrange({ filter: hashFilter });
      }

    } else {

      if (!isIsotopeInit) {
        imagesLoaded( grid_items, function() {
          grid = new Isotope( '.grid', {
            itemSelector: '.grid__item',
            layoutMode: 'fitRows',
            filter: hashFilter
          });
        });

        isIsotopeInit = true;
      }
    }

  }

  $(window).on( 'hashchange', onHashchange );

  // trigger event handler to init Isotope
  onHashchange();

})
