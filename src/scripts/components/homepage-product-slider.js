import { component } from 'picoapp'

import $ from 'jquery'
import 'slick-carousel'

export default component((node, ctx) => {
  const $slides = $(node).find(".homepage-product-slides-container");
  const $swatches = $(node).find(".homepage-product-slider-swatch");
  const $button = $(node).find(".homepage-product-slider-button");

  $($slides).slick({
    dots: false,
    arrows: false,
    draggable: true,
    swipe: true,
    fade: true
  });

  $swatches.click((event)=> {
    const $swatch = $(event.currentTarget);
    const index = $swatch.data("index");
    const url = $swatch.data("url");

    $swatches.removeClass("active");
    $swatch.addClass("active");

    $button.attr("href", url);
    $($slides).slick('slickGoTo', index);
  });
})
