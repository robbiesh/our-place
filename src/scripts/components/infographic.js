import { component } from 'picoapp'
import 'slick-carousel'
import $ from 'jquery'

export default component((node, ctx) => {
  $('.infographic-image-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    autoplay: true,
    autoplaySpeed: 3000,
    asNavFor: '.infographic-item-slider-container'
  });

  // On before slide change
  $('.infographic-image-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    $('.mobile-infographic-marker').css({
      top: $($('.infographic-image-slide').get(nextSlide)).data("markerYPosition"),
      left: $($('.infographic-image-slide').get(nextSlide)).data("markerXPosition")
    })
  });

  $('.infographic-item-slider-container').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    asNavFor: '.infographic-image-slider',
    dots: false,
    focusOnSelect: true,
    prevArrow: $('.infographic-prev'),
    nextArrow: $('.infographic-next')
  });
})
