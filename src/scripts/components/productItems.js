import { component } from 'picoapp'
import $ from 'jquery'
import 'slick-carousel'

export default component((node, ctx) => {

  var slider = node.querySelector('.js-items')
  var flkty = ''

  if (slider) {

    $('.js-items').slick({
      dots: false,
      arrows: false,
      draggable: true,
      swipe: true,
      infinite: false
    });

    // flkty = new Flickity(slider, {
    //   // options
    //   setGallerySize: false,
    //   imagesLoaded: true,
    //   watchCSS: true,
    //   contain: true,
    //   cellAlign: 'left'
    // });

    // slider.addEventListener('touchmove', function(e) { e.preventDefault(); }, { passive: false });
    //
    // flkty.on('dragStart', () => (document.ontouchmove = e => e.preventDefault()));
    // flkty.on('dragEnd', () => (document.ontouchmove = () => true));
  }

})
