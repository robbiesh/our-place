import { component } from 'picoapp'
import { addVariant } from '@/lib/cart.js'
import radio from '@/lib/radio.js'
import options from '@/lib/options.js'
import getProductJson from '@/lib/getProductJson.js'
import { formatMoney } from '@/lib/currency.js'

import Flickity from 'flickity'
import FlickityAsNavFor from 'flickity-as-nav-for'
import FlickityFade from 'flickity-fade'
import FlickityImagesLoaded from 'flickity-imagesloaded'

import 'slick-carousel'
import $ from 'jquery'

export default component(( node ) => {
  const opts = options(node)

  const option_title = node.querySelector('.js-option')
  const variant_price = node.querySelector('.js-price')

  var slider = node.querySelector('.js-pdp-slider')
  var flkty = ''

  $('.js-pdp-slider').slick({
    dots: false,
    arrows: true,
    draggable: true,
    swipe: true,
    prevArrow: $('.prev'),
    nextArrow: $('.next')
  });

  // if (slider) {
  //   flkty = new Flickity(slider, {
  //     // options
  //     setGallerySize: false,
  //     imagesLoaded: true,
  //     draggable: true,
  //     wrapAround: true,
  //     pageDots: false
  //   });
  //
  //   // slider.addEventListener('touchmove', function(e) { e.preventDefault(); }, { passive: false });
  //   //
  //   // flkty.on('dragStart', () => (document.ontouchmove = e => e.preventDefault()));
  //   // flkty.on('dragEnd', () => (document.ontouchmove = () => true));
  // }

  // var slider_nav = node.querySelector('.js-pdp-slider-nav')
  // var flkty_nav = ''
  //
  // if (slider_nav) {
  //   flkty_nav = new Flickity(slider_nav, {
  //     // options
  //     draggable: false,
  //     wrapAround: false,
  //     asNavFor: slider,
  //     pageDots: false,
  //     percentPosition: false,
  //     contain: true,
  //     cellAlign: 'left'
  //   });
  // }

  // cache
  getProductJson()

  opts.onUpdate(state => {
    getProductJson().then(json => {
      const variant = json.variants.filter(v => v.id == state.id)[0]

      // flkty.select(variant.position)

      // console.log(variant)

      // flkty.select(document.getElementById('v-' + variant.id).dataset.index)

      // console.log('v-' + variant.id)
      // console.log(document.getElementById('v-' + variant.id))

      // $(".js-pdp-slider").slick('slickGoTo', document.getElementById('v-' + variant.id).dataset.index)

      // $('.variant-item').hide();
      // $('.variant-item--' + variant.id).show();
      //
      // if (variant.available) {
      //   $('.js-add-to-cart').show();
      // } else {
      //   $('.js-add-to-cart').hide();
      // }

      option_title.innerHTML = variant.title
      variant_price.innerHTML = formatMoney(variant.price, '${{amount_no_decimals}}')
    })
  })

})
