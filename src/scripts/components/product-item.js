import { component } from 'picoapp'
import Flickity from 'flickity'
import FlickityAsNavFor from 'flickity-as-nav-for'
import FlickityFade from 'flickity-fade'
import FlickityImagesLoaded from 'flickity-imagesloaded'

export default component((node, ctx) => {

  var slider = node.querySelector('.js-product-slider')
  var flkty = ''

  if (slider) {
    flkty = new Flickity(slider, {
      // options
      setGallerySize: false,
      fade: true,
      imagesLoaded: true
    });

    // slider.addEventListener('touchmove', function(e) { e.preventDefault(); }, { passive: false });
    //
    // flkty.on('dragStart', () => (document.ontouchmove = e => e.preventDefault()));
    // flkty.on('dragEnd', () => (document.ontouchmove = () => true));
  }

  var navs = node.querySelectorAll('.js-product-image-nav')

  for (let i = 0; i < navs.length; i++) {
    navs[i].addEventListener('click', e => {
      e.preventDefault()
      flkty.select(navs[i].dataset.index)

      for (let j = 0; j < navs.length; j++) {
        navs[j].classList.remove('active')
      }

      navs[i].classList.add('active')

    })
  }

})
