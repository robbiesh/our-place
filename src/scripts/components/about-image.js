import { component } from 'picoapp'

import {TweenMax, Power2, TimelineLite} from "gsap/TweenMax"
import ScrollMagic from 'scrollmagic'
import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'


export default component((node, ctx) => {

  var tween = TweenMax.to("#about-image", 1, {scale: 1.2});

  var controller = new ScrollMagic.Controller();


  new ScrollMagic.Scene({triggerElement: "#about-image", duration: "300%", offset: "-500"})
    .setTween(tween)
		// .setClassToggle("#slider", "active") // add class toggle
    // .triggerHook(0)
		.addTo(controller);



})
