import { component } from 'picoapp'
import { addVariant } from '@/lib/cart.js'
import radio from '@/lib/radio.js'
import options from '@/lib/options.js'
import getProductJson from '@/lib/getProductJson.js'
import { formatMoney } from '@/lib/currency.js'

import Flickity from 'flickity'
import FlickityAsNavFor from 'flickity-as-nav-for'
import FlickityFade from 'flickity-fade'
import FlickityImagesLoaded from 'flickity-imagesloaded'

import 'slick-carousel'
import $ from 'jquery'

export default component(( node ) => {
  const opts = options(node)

  const option_title = node.querySelector('.js-option')
  const variant_price = node.querySelector('.js-price')
  const colorSelectors = $('input[type=radio][name=Color]');
  const initialVariantId = node.dataset.initialVariantId;
  const redirectVariant = node.dataset.redirectVariant;

  if(redirectVariant === "true") {
    window.location.search = `?variant=${initialVariantId}`;
  }

  var slider = node.querySelector('.js-pdp-slider')
  var flkty = ''

  $('.js-pdp-slider').slick({
    dots: false,
    arrows: true,
    draggable: true,
    swipe: true,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
    waitForAnimate: false,
    infinite: true
  });

  $(".js-pdp-slider").slick('slickFilter',function(index, slide){
    return $(slide).find(".pdp-slide").attr("id") == 'v-' + initialVariantId;
  });

  // On before slide change
  $('.js-pdp-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    let currentVariantIdSelector = $(slick.$slides[nextSlide]).find(".pdp-slide").attr("id").split("-")[1];
    $('.image-thumbnail').removeClass("active");
    $($(`.image-thumbnail.variant-item--${currentVariantIdSelector}`).get(nextSlide)).addClass("active");
  });

  colorSelectors.change((evenet) => {
    const $selectedColor = $(event.currentTarget);
    const isPreorder = $selectedColor.data("preorder") === true;

    if(isPreorder){
      $(".js-preorder-cta-text").show()
      $(".js-default-cta-text").hide()
    }else {
      $(".js-default-cta-text").show()
      $(".js-preorder-cta-text").hide()
    }
  })

  // if (slider) {
  //   flkty = new Flickity(slider, {
  //     // options
  //     setGallerySize: false,
  //     imagesLoaded: true,
  //     draggable: true,
  //     wrapAround: true,
  //     pageDots: false
  //   });
  //
  //   // slider.addEventListener('touchmove', function(e) { e.preventDefault(); }, { passive: false });
  //   //
  //   // flkty.on('dragStart', () => (document.ontouchmove = e => e.preventDefault()));
  //   // flkty.on('dragEnd', () => (document.ontouchmove = () => true));
  // }

  // var slider_nav = node.querySelector('.js-pdp-slider-nav')
  // var flkty_nav = ''
  //
  // if (slider_nav) {
  //   flkty_nav = new Flickity(slider_nav, {
  //     // options
  //     draggable: false,
  //     wrapAround: false,
  //     asNavFor: slider,
  //     pageDots: false,
  //     percentPosition: false,
  //     contain: true,
  //     cellAlign: 'left'
  //   });
  // }


  // cache
  getProductJson()

  $('.image-thumbnail').click(function(event) {
    let $currentThumbnail = $(event.currentTarget);
    let index = $currentThumbnail.data("index");
    $(".js-pdp-slider").slick('slickGoTo', index);
  })

  opts.onUpdate(state => {
    getProductJson().then(json => {
      const variant = json.variants.filter(v => v.id == state.id)[0]
      // flkty.select(variant.position)

      // console.log(variant)

      // flkty.select(document.getElementById('v-' + variant.id).dataset.index)
      $(".js-pdp-slider").slick('slickUnfilter');
      $(".js-pdp-slider").slick('slickFilter',function(index, slide){
        return $(slide).find(".pdp-slide").attr("id") == 'v-' + variant.id;
      });
      $(".js-pdp-slider").slick('slickGoTo', 0, true)

      $('.image-thumbnail').removeClass("active");

      $('.variant-item').hide();
      $('.variant-item--' + variant.id).show();
      $('.variant-item--' + variant.id).first().addClass("active");


      if (variant.available) {
        $('.js-add-to-cart').show();
      } else {
        $('.js-add-to-cart').hide();
      }

      option_title.innerHTML = variant.title
      variant_price.innerHTML = formatMoney(variant.price, '${{amount_no_decimals}}')
    })
  })

})
