import { component } from 'picoapp'
import { addVariant } from '@/lib/cart.js'
// import { Accordion } from 'accordion'
// import Accordion from 'accordion-js';
import Flickity from 'flickity'
import $ from 'jquery'

export default component((node, ctx) => {
  const json = JSON.parse(node.querySelector('.js-product-json').innerHTML)
  const form = node.querySelector('form')

  const { selectedOrFirstAvailableVariant, product } = JSON.parse(document.querySelector('.js-product-json').innerHTML)
  let currentVariant = product.variants.filter(v => v.id === selectedOrFirstAvailableVariant)[0]

  form.addEventListener('submit', e => {
    e.preventDefault()

    console.log(product.variants.length)

    if (product.variants.length > 1) {
      currentVariant = product.variants.filter(v => v.id === parseInt(form.elements.id.value))[0]
    }

    addVariant(currentVariant, form.elements.quantity.value)
    // console.log(json)
  })

  const trigger = document.getElementById('trigger').offsetTop
  const prodbar = document.getElementById('prodbar')
  // const placeholder = document.getElementById('placeholder')

  const trigger2 = document.getElementById('trigger').offsetTop + 200
  const trigger3 = document.getElementById('trigger').offsetTop + 400

  window.onscroll = function() {
    if (prodbar) {
      checkPosition()
    }
  };

  function checkPosition() {
    if (document.body.scrollTop > trigger || document.documentElement.scrollTop > trigger) {
      prodbar.classList.add('active');
    } else {
      prodbar.classList.remove('active');
    }

    if (document.body.scrollTop > trigger2 || document.documentElement.scrollTop > trigger2) {
      prodbar.classList.add('active2');
    } else {
      prodbar.classList.remove('active2');
    }

    if (document.body.scrollTop > trigger3 || document.documentElement.scrollTop > trigger3) {
      prodbar.classList.add('active3');
    } else {
      prodbar.classList.remove('active3');
    }
  }

  $('.js-accordion-toggle').click(function() {
    $(this).next('.js-accordion-body').slideToggle();
    $(this).toggleClass('open')
  })

  var h = $('.prodbar-spacer').height();
  $('.prodbar-spacer').css('min-height', h);

  var index = 0;
  var keywords = node.querySelectorAll('.js-keyword')
  var total = keywords.length


  if (total > 0) {
    setInterval(function() {

      for (var i = 0; i < total; i++) {
        keywords[i].classList.remove('active')
      }

      keywords[index].classList.add('active')

      index = (index + 1) % total

    }, 2000);
  }
})
