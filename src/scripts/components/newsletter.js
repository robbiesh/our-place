import { component } from 'picoapp'
import subscribe from 'klaviyo-subscribe'

export default component((node) => {
  var form = node
  const listId = form.getAttribute('data-list-id')

  form.addEventListener('submit', e => {
    e.preventDefault()

    const email = e.target.elements.email.value

    if (!email) return

    subscribe(listId, email, {}).then(res => {
      form.classList.add('was-successful')
      form.reset()

      setTimeout(() => {
        // form.classList.remove('was-successful')
      }, 2000)
    })
  })
})
