import { component } from 'picoapp'

import {TweenMax, Power2, TimelineLite} from "gsap/TweenMax"
import ScrollMagic from 'scrollmagic'
import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'

export default component((node, ctx) => {

  var tween = TweenMax.to("#collection-banner-image", 1, {scale: 1.4});

  var controller = new ScrollMagic.Controller();

  new ScrollMagic.Scene({triggerElement: "#collection-banner", duration: "100%"})
    .setTween(tween)
		// .setClassToggle("#slider", "active") // add class toggle
    // .triggerHook(0)
		.addTo(controller);

})
