import { removeAddon, updateAddon } from '@/lib/cart.js'
import { component } from 'picoapp'
import * as cart from '@shopify/theme-cart'

export default component(( node, state ) => {

  const decrease = node.querySelector('.js-remove-single')
  const increase = node.querySelector('.js-add-single')
  const qtyContainer = node.querySelector('.js-single-quantity')
  var currentQty = node.querySelector('.js-single-quantity').innerHTML
  const id = node.getAttribute('data-id')
  const key = node.getAttribute('data-key')
  const cartCount = document.querySelector('.js-cart-count')
  const subtotal = document.querySelector('.js-subtotal')

  decrease.addEventListener('click', e => {
    e.preventDefault()

    var q = parseInt(currentQty) - 1


    cart.updateItem(key, {quantity: q}).then(state => {
      var item = state.items.find(item => item.key === key);
      // console.log(`The item with key '${key}' now has quantity ${item.quantity}`);

      if (q == 0) {
        node.style.display = 'none'
      }

      cartCount.innerHTML = state.item_count
      subtotal.innerHTML = "$" + formatMoney((state.total_price / 100))

      // console.log(state)

      if (state.item_count == 0) {
        document.querySelector('.cart__box--full').classList.remove('cart--show')
        document.querySelector('.cart__box--empty').classList.add('cart--show')
      }

      if (item) {
        currentQty = item.quantity
        qtyContainer.innerHTML = item.quantity
      }
    });
  })

  increase.addEventListener('click', e => {
    e.preventDefault()

    var q = parseInt(currentQty) + 1

    cart.updateItem(key, {quantity: q}).then(state => {
      var item = state.items.find(item => item.key === key);


      currentQty = item.quantity
      qtyContainer.innerHTML = item.quantity

      console.log(state)

      cartCount.innerHTML = state.item_count
      subtotal.innerHTML = "$" + formatMoney((state.total_price / 100))
    })

  })

  function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
      j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };

})
