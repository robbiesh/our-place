import { component } from 'picoapp'
import Flickity from 'flickity'
import 'flickity-imagesloaded'
import 'slick-carousel'
import $ from 'jquery'

export default component((node, ctx) => {

  $(".js-press-bar").slick({
    infinite: true,
    slidesToShow: 2,
    adaptiveHeight: false,
    autoplay: false,
    arrows: false,
    dots: true,
    draggable: true,
    swipe: true
  });

  // var slider = new Flickity( '.js-press-bar', {
  //   pageDots: true,
  //   draggable: true,
  //   dragThreshold: 1,
  //   pauseAutoPlayOnHover: false,
  //   prevNextButtons: false,
  //   wrapAround: true,
  //   autoPlay: 3000,
  //   imagesLoaded: true
  // });

})
