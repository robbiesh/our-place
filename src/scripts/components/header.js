import { component } from 'picoapp'
import cookie from 'js-cookie'
import $ from 'jquery'

export default component((node, ctx) => {
  const cartCount = node.querySelector('.js-cart-count')
  const cartToggles = node.querySelectorAll('.js-cart-drawer-toggle')

  const mmToggle = document.querySelector('.js-mm-toggle')
  const mmClose = document.querySelector('.js-mm-close')
  const mmOverlay = document.querySelector('.js-mm-overlay')
  const mm = document.querySelector('.js-mm')

  const mmItems = document.querySelectorAll('.js-menu-link')

  const promoBar = node.querySelector('.promo-bar')
  const closePromo = node.querySelector('.js-promo-close')

  const visible = cookie.get('promo')
  // console.log(visible)

  var header = document.getElementById("header")

  var body = document.getElementById('body')

  const bod = document.getElementById('body')

  $('.header-item--shop').mouseenter(function() {
    $('.js-header-bottom').fadeIn();
    body.classList.add('header-dropdown--open')
    header.classList.add('header-dropdown--bg--open')
  });

  $('.header-item--always-pan').mouseenter(function() {
    $('.js-header-bottom').fadeOut();
    body.classList.remove('header-dropdown--open')
    header.classList.remove('header-dropdown--bg--open')
  });

  $('.js-header-bottom').mouseleave(function() {
    $('.js-header-bottom').fadeOut();
    body.classList.remove('header-dropdown--open')
    header.classList.remove('header-dropdown--bg--open')
  });

  // run test on initial page load
  checkSize();

  // run test on resize of the window
  $(window).resize(checkSize);


  //Function to the css rule
  function checkSize(){
      if ($(".header--trigger").css("float") == "left" ){
        $(".js-header-top").mouseenter(function() {
          body.classList.add('header--hover')
          header.classList.add('header--bg--hover')
        });

        $(".js-header-top").mouseleave(function() {
          body.classList.remove('header--hover')
          header.classList.remove('header--bg--hover')

        });
      } else {
        $(".js-header-top").off('mouseenter')
        $(".js-header-top").off('mouseleave')

      }
  }


  if (!visible) {
    setTimeout(() => {
      bod.classList.add('has-promo-bar')
    }, 100)
  }

  closePromo.addEventListener('click', e => {
    e.preventDefault()
    promoBar.classList.add('promo-bar-hidden')
    bod.classList.remove('has-promo-bar')
    cookie.set('promo', true)
  })

  mmToggle.addEventListener('click', e => {
    e.preventDefault()
    mm.classList.add('mm--open')
  })

  mmClose.addEventListener('click', e => {
    e.preventDefault()
    mm.classList.remove('mm--open')
  })

  mmOverlay.addEventListener('click', e => {
    e.preventDefault()
    mm.classList.remove('mm--open')
  })

  for (let i = 0; i < cartToggles.length; i++) {
    cartToggles[i].addEventListener('click', e => {
      e.preventDefault()

      ctx.emit('cart:toggle', state => {
        return {
          cartOpen: !state.cartOpen
        }
      })
    })
  }


  for (let i = 0; i < mmItems.length; i++) {
    mmItems[i].addEventListener('click', e => {
      if (window.location.href == mmItems[i].href) {
        mm.classList.remove('mm--open')
      }
    })
  }


  ctx.on('cart:updated', state => {
    cartCount.innerHTML = state.cart.item_count
  })

  cartCount.innerHTML = ctx.getState().cart.item_count


  var prevScrollpos = window.pageYOffset


  window.addEventListener('scroll', function() {

		var currentScrollPos = window.pageYOffset;


		if (currentScrollPos > 10) {
      // body.classList.add('header--bg')

			if (prevScrollpos > currentScrollPos) {
        body.classList.remove('header--hidden')
        body.classList.add('header--open')
        header.classList.add('header--bg')
			} else {
        body.classList.add('header--hidden')
        body.classList.remove('header--open')
        header.classList.remove('header--bg')
			}
		} else {
      body.classList.remove('header--hidden')
      body.classList.remove('header--open')
      header.classList.remove('header--bg')
    }

	  prevScrollpos = currentScrollPos;

	})

})
