import { picoapp } from 'picoapp'

import slaterWelcome from '@/components/slater-welcome.js'

import header from '@/components/header.js'
import homepageProductSlider from '@/components/homepage-product-slider.js'
import productSelection from '@/components/product-selection.js'
import productSelectionGC from '@/components/product-selection-gc.js'
import cartDrawer from '@/components/cartDrawer.js'
import cartDrawerItem from '@/components/cartDrawerItem.js'
import accountLogin from '@/components/accountLogin.js'
import product from '@/components/product.js'
import slider from '@/components/slider.js'
import collectionSlider from '@/components/collection-slider.js'
import aboutSlider from '@/components/about-slider.js'
import productItem from '@/components/product-item.js'
import productBundle from '@/components/product-bundle.js'
import productItems from '@/components/productItems.js'
import banner from '@/components/banner.js'
import video from '@/components/video.js'
import instagram from '@/components/instagram.js'
import faqs from '@/components/faqs.js'
import cart from '@/components/cart.js'
import footer from '@/components/footer.js'
import newsletterSlide from '@/components/newsletterSlide.js'
import newsletterForm from '@/components/newsletter.js'
import image from '@/components/image.js'
import infographic from '@/components/infographic.js'
import aboutContent from '@/components/about-content.js'
import collectionBanner from '@/components/collection-banner.js'
import aboutImage from '@/components/about-image.js'
import pressBar from '@/components/press-bar.js'
import pressSlider from '@/components/press-slider.js'
import filters from '@/components/filters.js'
import cluster from '@/components/cluster.js'
import productNB from '@/components/product-nb.js'
import productReviewsHeader from '@/components/product-reviews-header.js'

const state = {
  cartOpen: false
}

const components = {
  slaterWelcome,
  accountLogin,
  header,
  homepageProductSlider,
  productSelection,
  productSelectionGC,
  cartDrawer,
  cartDrawerItem,
  product,
  slider,
  collectionSlider,
  aboutSlider,
  productItem,
  productBundle,
  productItems,
  banner,
  video,
  instagram,
  faqs,
  cart,
  footer,
  newsletterSlide,
  image,
  infographic,
  newsletterForm,
  aboutContent,
  collectionBanner,
  aboutImage,
  pressBar,
  pressSlider,
  filters,
  cluster,
  productNB,
  productReviewsHeader
}

export default picoapp(components, state)
