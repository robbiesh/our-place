import operator from 'operator'
import analytics from './lib/analytics.js'
import getProductJson from './lib/getProductJson.js'

function productTracking (slug) {
  getProductJson(slug).then(json => {
    if (!json) return

    const item = {
      ProductName: json.title,
      ProductID: json.id,
      ImageURL: json.featured_image,
      URL: json.url,
      Brand: 'Our Place',
      Price: json.price / 100,
      CompareAtPrice: json.compare_at_price
    };

    _learnq = _learnq || []

    _learnq.push(['track', 'Viewed Product', item]);

    _learnq.push(['trackViewedItem', {
      Title: item.ProductName,
      ItemId: item.ProductID,
      Categories: item.Categories,
      ImageUrl: item.ImageURL,
      Url: item.URL,
      Metadata: {
        Brand: item.Brand,
        Price: item.Price,
        CompareAtPrice: item.CompareAtPrice
      }
    }]);

    analytics({
      ga: ['ec:addImpression', {
        id: item.sku,
        name: item.title,
        price: item.price / 100,
        category: item.product_type,
        brand: item.vendor
      }],
      fbq: ['track', 'ViewContent', {
        value: item.price / 100,
        currency: 'USD',
        contents: {
          id: item.sku,
          item_price: item.price / 100
        }
      }]
    })
  })
}

/**
 * operator is a tiny "PJAX" library, please have a look at the docs for
 * more info
 *
 * @see https://github.com/estrattonbailey/operator
 */
// const router = operator('#root', [
//   {
//     path: '/products/:slug',
//     handler (state) {
//       // subsequent views, separate from initial
//       productTracking(state.params.slug)
//     }
//   },
//   {
//     path: '/checkout',
//     ignore: true
//   },
//   {
//     path: '/pages/terms',
//     ignore: true
//   },
//   {
//     path: '/pages/privacy',
//     ignore: true
//   },
//   /**
//    * creates a page transition
//    * @see https://github.com/estrattonbailey/operator#transition-animation
//    */
//   () => new Promise(res => {
//     document.body.classList.add('is-transitioning')
//     setTimeout(res, 200)
//     setTimeout(() => document.body.classList.remove('is-transitioning'), 300)
//
//
//
//   })
// ])
//
// router.on('after', ({ title, pathname }) => {
//   document.title = title
//   window.history.pushState({}, '', pathname)
//
//   analytics({
//     ga: ['set', 'page', pathname],
//     ga: ['send', 'pageview'],
//     fbq: ['track', 'PageView']
//   })
// })
//
//
//
// export default router
