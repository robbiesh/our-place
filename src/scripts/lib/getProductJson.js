const cache = {}

export default function getProductJson (
  slug = window.location.pathname,
  query = window.location.search,
  opts = {}
) {
  console.log("eeelo");
  if (cache[slug] && !opts.refetch) return Promise.resolve(cache[slug])
  return fetch(window.location.origin + slug + '.js' + query)
    .then(res => res.json())
    .then(product => {
      cache[slug] = product
      return product
    })
    .catch(e => {
      return null
    })
}
