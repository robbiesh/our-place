window.BOLD = window.BOLD || {}
window.BOLD.checkout = window.BOLD.checkout || {}
window.BOLD.checkout.isCheckoutLink = function(elem){
  return elem.matches("[href*=checkout]:not([href*='/tools/checkout'])") && !elem.className.match(new RegExp('(\\s|^)bold_clone(\\s|$)'));
}
window.BOLD.checkout.doCheckout = function(ev) {
  ev && (ev.preventDefault(), ev.stopPropagation());
  var e = document.createElement("FORM");
  e.method = "post",
    e.action = "/apps/checkout/begin-checkout" + googleAnalyticsGetParamString();
  var n = BOLD.checkout.getCookie("cart")
  , r = document.createElement("INPUT");
  r.type = "HIDDEN",
    r.name = "cart_id",
    r.value = n,
    e.appendChild(r),
    document.body.appendChild(e)
  if (BOLD && BOLD.common && typeof BOLD.common.eventEmitter === 'object' && typeof BOLDCURRENCY !== 'undefined') {
    BOLD.common.eventEmitter.emit('BOLD_CASHIER_checkout', {target: e});
  }
  e.submit()
}
document.addEventListener('click', function(e){
  if( (BOLD.checkout.isCheckoutLink(e.target) || BOLD.checkout.isCheckoutLink(e.target.parentElement) ) && BOLD.checkout.isEnabled()){
  	BOLD.checkout.doCheckout(e)
  }
}, true);